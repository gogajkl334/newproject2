FROM python:3.6-alpine3.9

WORKDIR /flask

COPY hits . ./

COPY requirements.txt . 

RUN apk update

RUN pip install -r requirements.txt 

EXPOSE 5000

ENTRYPOINT ["python"]
CMD ["app.py"]
